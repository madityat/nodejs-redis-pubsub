const redis = require('redis');

(async () => {

  const client = redis.createClient();

  const subscriber = client.duplicate();

  await subscriber.connect();

  await subscriber.subscribe('notification', (message) => {
    console.log(message); // 'message'
  });
  
  console.log('Subscriber initialized');
})();