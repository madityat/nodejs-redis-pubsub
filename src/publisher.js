const redis = require('redis');
const publisher = redis.createClient();

(async () => {

  const notification = {
    notificationId: '123456',
    title: 'New Notification',
    message: 'lorem ipsum dolor sit amet',
  };

  await publisher.connect();

  const sendResult = await publisher.publish('notification', JSON.stringify(notification));
  console.log(`Success sending notification to ${sendResult} subscribers`);
})();